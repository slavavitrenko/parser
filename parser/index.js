/**
* Created by slava on 18.02.17.
*/


'use strict';

require('./utils/overridings');
require('dotenv').config({path: './.env'});


const parser = require('./utils/parser'),
process = require('process'),
logger = require('./utils/logger').log,
express = require('express'),
bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.json());

let http = require('http').Server(app);
let io = require('socket.io')(http);

let sockets = [];

io.on('connection', (socket) => {
    logger('User connected: ' + socket.id);
    sockets.insert(socket.id, socket);
    socket.emit('status', parser.status());
    socket.on('disconnect', () => {
        delete sockets[socket.id];
        logger('user disconnected: ' + socket.id);
    });
    socket.on('start', () => {
        parser.start();
        socket.emit('status', parser.status());
    });
    socket.on('stop', () => {
        parser.stop();
        socket.emit('status', parser.status());
    });
    socket.on('reset', () => {
        parser.clearCounters();
    });
});


app.get('/', (req, res) => {
    res.end(parser.status());
})
.get('/start', (req, res) => {
    res.end('success');
    parser.start();
})
.get('/stop', (req, res) => {
    res.end('success');
    parser.stop();
})
.post('/', (req, res) => {
    parser.reloadLists(req.body.lists || {});
    logger('Lists accepted');
    res.end('success');
});

http.listen(process.env.PORT, () => {
    logger('Listening at port ' + process.env.PORT);
});


parser.on('tick', () => {
    sockets.forEach(function (s) {
        s.emit('status', parser.status());
    });
});


parser.loadLists();

let numWorkers = process.env.workers;

setInterval(parser.parse, 1000 / numWorkers);
