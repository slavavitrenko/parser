/**
 * Created by slava on 18.02.17.
 */

Array.prototype.replaceArray = (find, replace) => {
    let replaceString = this;
    let regex;
    for (let i = 0; i < find.length; i++) {
        regex = new RegExp(find[i], "g");
        replaceString = replaceString.replace(regex, replace[i]);
    }
    return replaceString;
};

let logger = require('./logger').log,
mysql = require('mysql'),
process = require('process'),
request = require('request'),
dotenv = require('dotenv').config({path: './.env'}),
events = require('events'),
lists = require('./lists');

require('./overridings');

let channel = new events.EventEmitter();

const db = mysql.createConnection({
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_NAME
});


let p = {
    login: '',
    started: true,
    rejects: 0,
    totalRequests: 0,
    order: 'id asc',
    inWork: 0,
    workers: 20,
    lists: lists
};

p.loadLists = () => {
    p.lists.loadList();
};

p.reloadLists = (lists) => {
    p.lists.reload(lists);
    return;
}

p.start = () => {
    if(p.started == false){
        p.started = true;
        channel.emit('tick');
        logger('started');
    }
};

p.stop = () => {
    if(p.started == true){
        p.started = false;
        channel.emit('tick');
        logger('stopped');
    }
};

p.status = () => {
    return {
        current: p.login,
        status: p.started,
        rejects: Math.round(p.rejects / 2),
        requests: p.totalRequests,
        inWork: p.inWork
    };
};

p.clearCounters = () => {
    p.rejects = 0;
    p.totalRequests = 0;
    p.login = '';
    channel.emit('tick');
};

p.on = (name, cb) => {
    channel.on(name, cb);
};

p.selectUser = (cb) => {
    db.query('CALL `br`();', (err, result) => {
        if(result && result[0] && result[0][0] && result[0][0].instagram_login){
            cb(result[0]);
        }
        else{
            p.started = false;
            channel.emit('tick');
        }
    });
};

p.getData = (login, id, attempts, cb) => {
    p.totalRequests += 1;
    request.get(
        "https://instagram.com/" + login + "/?__a=1",
        (err, response, body) => {
            if(response && response.statusCode == 200){
                try{
                    body = JSON.parse(body);
                }
                catch(e){
                    // throw e;
                    body = {
                        user: {
                            full_name: "",
                            biography: ""
                        }
                    };
                }
                cb(body);
            }
            else{
                p.rejects += 1;
                p.inWork -= 1;
                channel.emit('tick');
                let checked = attempts > 2 ? '2' : '0';
                p.setAttempts(checked, id, attempts);
            }
        });
};

p.checkPhones = (body) => {

    if(!body.user || !body.user.biography || !body.user.full_name) return;

    let text = " " + body.user.biography + body.user.full_name;
    if(!text)return;
    text = text.replaceArray(
        ['0⃣', '1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣'],
        ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        );
    const regex = /(?:(?:[^0-9-]|^){1})(?:[7-9](?:[^0-9]{1,2})?)(?:\d.*?){9,17}/gm;
    let m;
    let phones = [];
    while ((m = regex.exec(text)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        m.forEach(function(match){
            if(!~match.indexOf(',')){
                let temp = '';
                for(let charIndex in match){
                    if(~['1','2','3','4','5','6','7','8','9','0'].indexOf(match[charIndex])){
                        temp += match[charIndex] + "";
                    }
                    if(temp.length > 1){
                        if(temp.substr(0, 1) == '8' && temp.substr(1, 1) == '9') { temp[0] = 7; }
                        if(temp.substr(0, 1) == '9' && temp.length == 10) { temp = '7' + temp; }
                    }
                }
                if(temp.length >= 11){
                    phones.push(temp.substring(0, 11));
                }
            }
        });
    }
    return phones;
};

p.clearLogin = (login, cb) => {
    db.query("delete from `logins` where `instagram_login`=?;",
        [login],
        () => { cb(); });
};

p.setAttempts = (checked, id, attempts) => {
    db.query("update `logins` "
        +"set `attempts`=?, `checked`=? "
        +"where `id`=?;",
        [attempts, checked, id], (err) => {
            // if(err) throw err;
        });
};

p.insertData = (body, user, phone) => {
    try {
        user['id'] = undefined;
        user['phone'] = phone || null;
        user['checked'] = 2;
        user['created_at'] = user.updated_at = Math.floor(Date.now() / 1000);
        user = p.lists.check('' + body.user.biography + body.user.full_name, user);
        user['instagram_id'] = body.user.id || null;
        user['followed_by'] = body.user.followed_by.count || null;
        user['media_count'] = body.user.media.count || null;
        user['follows_count'] = body.user.follows.count || null;
        user['external_url'] = body.user.external_url || null;

        db.query(
            "insert into users set ?;",
            user,
            (err) => {
                // if (err) throw err;
            });
    } catch(e){
        // throw e;
        logger('error while inserting data: ', user);
    }
};

p.setChecked = (id, cb) => {
    db.query("update `logins` set `checked`=2 where `id`=?", [id], (err) => {
        if(!err){
            cb();
        }
        else{
            // throw err;
            console.log('Set checked failed');
        }
    });
};

p.parse = () => {
    try {
        if(p.inWork >= (p.workers * 2)){
            logger('in work: ' + p.inWork + ', paused');
            return;
        }
        if (p.started === false) return;
        p.selectUser((result) => {
            let login = result[0].instagram_login,
                id    = result[0].id,
            attempts = result[0].attempts + 1;
            p.inWork += 1;
            logger(result[0].instagram_login);
            p.login = login;

            p.getData(login, id, attempts, (body) => {
                let phones = p.checkPhones(body) || [];
                p.inWork -= 1;
                channel.emit('tick');
                
                p.setChecked(id, () => {
                    if(phones.length > 0){
                        p.clearLogin(login, () => {
                            phones.forEach((item) => {
                                p.insertData(body, result[0], item);
                            });
                        });
                    } 
                });
            });
        });
    }catch(e){
        // throw e;
    }
};

module.exports = p;
