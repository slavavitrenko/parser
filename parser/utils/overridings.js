/**
 * Created by slava on 18.02.17.
 */

'use strict';

Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
};

String.prototype.replaceArray = function(find, replace) {
    let replaceString = this;
    let regex;
    for (let i = 0; i < find.length; i++) {
        regex = new RegExp(find[i], "g");
        replaceString = replaceString.replace(regex, replace[i]);
    }
    return replaceString;
};
