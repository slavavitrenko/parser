let jf = require('jsonfile');

let l = {
	black: [],
	silver: [],
	gold: []
};

l.loadList = () => {
	let lists = jf.readFileSync(__dirname + '/lists.txt');
	l.black = new Array(lists.black) || [];
	l.silver = new Array(lists.silver) || [];
	l.gold = new Array(lists.gold);
}

l.reload = (lists) => {
	l.save(lists);
	l.loadList();
}

l.check = (text, user) => {

	let rank = null;
	let word = null;

	l.black[0].forEach((item) => {
		if(~text.toLowerCase().indexOf(item.toLowerCase())){
			rank = 'black';
			word = item;
		}
	});

	if(rank == 'black'){

		user['rank'] = rank;
		user['word'] = word;

		return user;
	}

	l.gold[0].forEach((item) => {
		if(~text.toLowerCase().indexOf(item.toLowerCase())){
			rank = 'gold';
			word = item;
		}
	});

	l.silver[0].forEach((item) => {
		if(~text.toLowerCase().indexOf(item.toLowerCase())){
			rank = 'silver';
			word = item;
		}
	});

	user['rank'] = rank;
	user['word'] = word;

	return user;
}

l.save = (lists) => {
	jf.writeFileSync(__dirname + '/lists.txt', lists);
}


module.exports = l;